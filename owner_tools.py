import os
import time
import math
import numpy as np
import pandas as pd

import statsmodels.api as sm
import statsmodels.formula.api as smf

from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor, GradientBoostingRegressor, StackingRegressor
from sklearn.metrics import mean_squared_error, r2_score, accuracy_score, confusion_matrix, classification_report, \
    make_scorer
from scipy.stats import pearsonr


def string2list(token, data_type):
    data_list = None
    if data_type == 'float':
        data_list = [float(i) for i in token.replace(' ', '').split(',')]
    elif data_type == 'int':
        data_list = [int(i) for i in token.replace(' ', '').split(',')]
    elif data_type == 'str':
        data_list = [str(i) for i in token.replace(' ', '').split(',')]
    return data_list


def array2string(token):
    return str(list(token)).replace('[', '').replace(']', '').replace("'", "")


def df2file(filename, data=pd.DataFrame(), save=True):
    #     filename = 'rfe_test.csv'
    print("filename->", filename)
    if os.path.isfile(filename):
        data_df = pd.read_csv(filename)
    else:
        data_df = pd.DataFrame()

    if len(data_df) > 0:
        temp_df = pd.DataFrame(data)
        data_df = data_df.append(temp_df)
    else:
        data_df = data

    if save:
        data_df.to_csv(filename, index=False)
    else:
        data_df = pd.read_csv(filename)

    return data_df


def run_randomforestregressor(X_train, X_test, y_train, y_test, random_state):
    regs = RandomForestRegressor(n_estimators=100, random_state=random_state, n_jobs=-1)
    regs.fit(X_train, y_train)
    y_pred = regs.predict(X_test)
    correlation = pearsonr(y_test, y_pred)
    mse = mean_squared_error(y_test, y_pred)
    rsquared = r2_score(y_test, y_pred)
    rfr_score = {'rsquared': rsquared, 'correlation': correlation[0], 'mse': mse}
    print("RandomForestRegressor Score->", rfr_score)
    return rfr_score


def sklearn_vif(exogs, data):
    # initialize dictionaries
    vif_dict, tolerance_dict = {}, {}

    # form input data for each exogenous variable
    for exog in exogs:
        not_exog = [i for i in exogs if i != exog]
        X, y = data[not_exog], data[exog]

        # extract r-squared from the fit
        r_squared = LinearRegression().fit(X, y).score(X, y)

        # calculate VIF
        vif = 1 / (1 - r_squared)
        vif_dict[exog] = vif

        # calculate tolerance
        tolerance = 1 - r_squared
        tolerance_dict[exog] = tolerance

    # return VIF DataFrame
    df_vif = pd.DataFrame({'VIF': vif_dict, 'Tolerance': tolerance_dict})

    return df_vif


def pretty_print_linear(coefs, names=[], sort=False):
    """
    # print ("Lasso model: ", pretty_print_linear(lasso.coef_, names,))
    """
    if len(names) == 0:
        names = ["X%s" % x for x in range(len(coefs))]
    lst = zip(coefs, names)
    if sort:
        lst = sorted(lst, key=lambda x: -np.abs(x[0]))
    return " + ".join("%s * %s" % (round(coef, 3), name) for coef, name in lst)


def correlation_cols(X, y):
    out_list = []
    for column in X.columns:
        corr_tuple = pearsonr(X[column], y)
        col_corr = {"Features": column, "Correlation": corr_tuple[0], "Correlation_Abs": np.abs(corr_tuple[0]),
                    "P-Value": corr_tuple[1]}
        out_list.append(col_corr)
    return pd.DataFrame(out_list).sort_values(by='Correlation_Abs', ascending=False)


def get_vif(exogs, data):
    '''Return VIF (variance inflation factor) DataFrame

    Args:
    exogs (list): list of exogenous/independent variables
    data (DataFrame): the df storing all variables

    Returns:
    VIF and Tolerance DataFrame for each exogenous variable

    Notes:
    Assume we have a list of exogenous variable [X1, X2, X3, X4].
    To calculate the VIF and Tolerance for each variable, we regress
    each of them against other exogenous variables. For instance, the
    regression model for X3 is defined as:
                        X3 ~ X1 + X2 + X4
    And then we extract the R-squared from the model to calculate:
                    VIF = 1 / (1 - R-squared)
                    Tolerance = 1 - R-squared
    The cutoff to detect multicollinearity:
                    VIF > 10 or Tolerance < 0.1
    '''

    # initialize dictionaries
    vif_dict, tolerance_dict = {}, {}

    # create formula for each exogenous variable
    for exog in exogs:
        not_exog = [i for i in exogs if i != exog]
        formula = f"{exog} ~ {' + '.join(not_exog)}"

        # extract r-squared from the fit
        r_squared = smf.ols(formula, data=data).fit().rsquared

        # calculate VIF
        vif = 1 / (1 - r_squared)
        vif_dict[exog] = vif

        # calculate tolerance
        tolerance = 1 - r_squared
        tolerance_dict[exog] = tolerance

    # return VIF DataFrame
    df_vif = pd.DataFrame({'VIF': vif_dict, 'Tolerance': tolerance_dict}).sort_values(by=['VIF'], ascending=False)

    return df_vif


def sklearn_vif(exogs, data):
    # initialize dictionaries
    vif_dict, tolerance_dict = {}, {}

    # form input data for each exogenous variable
    for exog in exogs:
        not_exog = [i for i in exogs if i != exog]
        X, y = data[not_exog], data[exog]

        # extract r-squared from the fit
        r_squared = LinearRegression().fit(X, y).score(X, y)

        # calculate VIF
        vif = 1 / (1 - r_squared)
        vif_dict[exog] = vif

        # calculate tolerance
        tolerance = 1 - r_squared
        tolerance_dict[exog] = tolerance

    # return VIF DataFrame
    df_vif = pd.DataFrame({'VIF': vif_dict, 'Tolerance': tolerance_dict}).sort_values(by=['VIF'], ascending=False)

    return df_vif


def findoutliers(column):
    outliers = []
    Q1 = column.quantile(.25)
    Q3 = column.quantile(.75)
    IQR = Q3 - Q1
    lower_limit = Q1 - (1.5 * IQR)
    upper_limit = Q3 + (1.5 * IQR)
    for out1 in column:
        if out1 > upper_limit or out1 < lower_limit:
            outliers.append(out1)
    return np.array(outliers)


def convert_char_tr2en(token):
    if isinstance(token, str):
        new_token = token.replace('ş', 's').replace('Ş', 'S').replace('ç', 'c').replace('Ç', 'C') \
            .replace('ı', 'i').replace('İ', 'I').replace('ğ', 'g').replace('Ğ', 'G') \
            .replace('ö', 'o').replace('Ö', 'O').replace('ü', 'u').replace('Ü', 'U') \
            .replace('ý', 'i').replace('Ý', 'I').replace('ð', 'g') \
            .replace('Ð', 'G').replace('þ', 's').replace('Þ', 'S').replace('z', 'z').replace(' ', '_')
        new_token = new_token.lower()
        return new_token


def results_summary_to_dataframe(results):
    '''take the result of an statsmodel results table and transforms it into a dataframe'''
    pvals = results.pvalues
    coeff = results.params
    conf_lower = results.conf_int()[0]
    conf_higher = results.conf_int()[1]

    results_df = pd.DataFrame({
        "coeff": coeff,
        "pvals": pvals,
        "conf_lower": conf_lower,
        "conf_higher": conf_higher
    })
    results_df = results_df.reset_index().reindex().rename({"index":'column_name'},axis=1)
    return results_df